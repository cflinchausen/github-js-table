import React, { Component } from 'react';

import './Footer.css';

class Footer extends Component {
  constructor(){
    super();

    this.renderItem = this.renderItem.bind(this);
  }

  renderItem(item, index){
    const current = index === this.props.current;

    return (
      <li 
        key={index} 
        className={current ? 'current' : ''}
        onClick={() => this.props.setPage(index)}>
        {index + 1}
      </li>
    );
  }

  render() {
    return (
      <ul className="Footer">
        {this.props.items.map(this.renderItem)}                
      </ul>
    );
  }
}

export default Footer;
