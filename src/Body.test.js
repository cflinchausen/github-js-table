import React from 'react';
import ReactDOM from 'react-dom';
import Body from './Body';

it('renders without crashing', () => {
  const table = document.createElement('table');
  ReactDOM.render(<Body items={[{name: 'test', description: 'test', stars: 1, url: ''}]} />, table);
});
