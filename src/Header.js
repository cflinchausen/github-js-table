import React, { Component } from 'react';

import './Header.css';

class Header extends Component {
  render() {
    return (
      <thead className="Header">
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Stars</th>
          <th>Forks</th>
        </tr>
      </thead>
    );
  }
}

export default Header;
