import React, { Component } from 'react';
import Table from './Table';
import Footer from './Footer';
import Config from './Config';

import './App.css';

class App extends Component {

  constructor() {
    super();

    this.setPage = this.setPage.bind(this);

    this.state = {
      pages: [],
      currentPage: 0,
      currentItems: [],
      loaded: false
    }
  }

  componentDidMount() {
    fetch(Config.endpoint)
      .then(response => {
        if(!response.ok) throw Error(response.statusText);
        return response;
      })
      .then(response => response.json())
      .then(data => {
        const pages = [];
        const numberOfPages = Config.perPage / Config.itemsToShowPerPage;

        const items = data.items
          .slice(0, 100)
          .map(i => {
            return {
              name: i.name,
              description: i.description,
              url: i.html_url,
              stars: i.stargazers_count,
              forks: i.forks
            };
          });

        for (let i = 0; i < numberOfPages; i++) {
          pages.push(items.slice(i * Config.itemsToShowPerPage, (i + 1) * Config.itemsToShowPerPage));
        }

        this.setState({
          pages: pages,
          loaded: true,
          currentItems: pages[0]
        });
      })
      .catch(error => {
        this.setState({
          error: error.message
        })
      });;
  }

  setPage(page) {
    const currentItems = this.state.pages[page || 0];
    this.setState({ currentPage: page, currentItems });
  }

  render() {
    if (this.state.loaded) {
      return (

        <div className="App">
          <h1>Top 100 JavaScript repositories on GitHub.</h1>
          <Table
            items={this.state.currentItems} />
          <Footer
            setPage={this.setPage}
            items={this.state.pages}
            current={this.state.currentPage} />
        </div>
      );
    } else if(this.state.error) {
      return (
        <div className="App">
          {this.state.error}
        </div>
      )
    }else {
      return (
        <div className="App">
          Loading repositories...
        </div>
      );
    }
  }
}

export default App;
