import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';

it('renders without crashing', () => {
  const table = document.createElement('table');
  ReactDOM.render(<Header />, table);
});
