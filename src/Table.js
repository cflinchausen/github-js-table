import React, { Component } from 'react';
import Header from './Header';
import Body from './Body';

import './Table.css';

class Table extends Component {
  render() {
    return (
      <table className="Table">
        <Header />
        <Body items={this.props.items} />
      </table>
    );
  }
}

export default Table;
