import React from 'react';
import ReactDOM from 'react-dom';
import Footer from './Footer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Footer setPage={''} items={[{name: 'test', description: 'test', stars: 1, url: ''}]} current={1}/>, div);
});
