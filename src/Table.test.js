import React from 'react';
import ReactDOM from 'react-dom';
import Table from './Table';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Table items={[{name: 'test', description: 'test', stars: 1, url: ''}]} />, div);
});
