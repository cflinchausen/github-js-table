import React, { Component } from 'react';

import './Body.css';

class Body extends Component {

  constructor() {
    super();

    this.renderItem = this.renderItem.bind(this);
  }

  renderItem(item) {
    return (
      <tr key={item.name}>
        <td><a href={item.url} target="_blank">{item.name}</a></td>
        <td>{item.description}</td>
        <td>{item.stars}</td>
        <td>{item.forks}</td>
      </tr>
    );
  }

  render() {
    return (
      <tbody className="Body">
        {this.props.items.map(this.renderItem)}
      </tbody>
    );
  }
}

export default Body;
