const Config = {
  itemsToShowPerPage: 20,
  perPage: 100,
  endpoint: 'https://api.github.com/search/repositories?q=language:javascript&sort=stars&order=desc&per_page=100'
}

export default Config;
