# Readme
Built using React and Create React App
See https://cflinchausen.gitlab.io/github-js-table/ for live example

## How to run
* Clone or download repository
* `npm install` (**You’ll need to have Node >= 6 on your machine**)
* `npm start`